# OpenML dataset: SyskillWebert-Bands

https://www.openml.org/d/380

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Michael Pazzani (pazzani@ics.uci.edu)  
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Syskill+and+Webert+Web+Page+Ratings)- 1999  
**Please cite**:   

**Syskill and Webert Web Page Ratings**  
This database contains the HTML source of web pages plus the ratings of a single user on these web pages. The web pages are on four separate subjects (Bands- recording artists; Goats; Sheep; and BioMedical)

The HTML source of a web page is given. Users looked at each web page and indicated on a 3 point scale (hot medium cold) 50-100 pages per domain. However, this is realistic because we want to learn user profiles from as few examples as possible so that users have an incentive to rate pages.

The problem is to predict user ratings for web pages (within a subject category). The accuracy of predicting ratings is reported in early publications. Later publications used the precision at top N or the F-measure.

**Past Usage**  
Pazzani M., Billsus, D. (1997). Learning and Revising User Profiles: The identification of interesting web sites. Machine Learning 27, 313-331

Pazzani, M., Muramatsu J., Billsus, D. (1996). Syskill & Webert: Identifying interesting web sites. Proceedings of the National Conference on Artificial Intelligence, Portland, OR.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/380) of an [OpenML dataset](https://www.openml.org/d/380). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/380/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/380/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/380/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

